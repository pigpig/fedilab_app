*[Home](../home) &gt;* UntrackMe
<div align="center">
<img src="../res/untrackme_icon.png" width="70px">
<b>UntrackMe</b>
</div>

## What does UntrackMe do?

Basically it handles urls. It can do these:

- [Redirect](#redirect):
    - _Transform YouTube, Twitter, Instagram and Google Maps URLs into URLs of front-ends and services that respect your privacy._
- [Unshorten](#unshorten):
    - _See the real link behind short URLs of some URL shortening services without loading the web page_
- [Cleanup](#cleanup):
    - _Remove known UTM parameters from a URLs_


## Two variants

There are two variants of UntrackMe. Both are free

- UntrackMe **Full**<br>
&nbsp;&nbsp;&nbsp;&nbsp;_Has all the features mentioned above_ <br><br>
- UntrackMe **Lite**<br>
&nbsp;&nbsp;&nbsp;&nbsp;_Only has 'Redirect' and 'Unshorten' features_

<br>

<a name="redirect"></a>
<h3>Features</h3>
<h4>Redirect</h4>

&nbsp;&nbsp;&nbsp;&nbsp;UntrackMe can redirect you to privacy friendly services as below
  - Youtube  -->  [Invidious](https://www.invidio.us/)
  - Twitter  -->  [Nitter](https://nitter.net/)
  - Instagram  -->  [Bibliogram](https://bibliogram.art/)
  - Google Maps -->  [OpenStreetMap](https://www.openstreetmap.org/)<br>
  *Optionally, you can use geo URIs instead of OSM, so you'll be able to use them with any other compatible app.*
<br><a name="unshorten"></a><br>
<h4>Unshorten</h4>

&nbsp;&nbsp;&nbsp;&nbsp;You can use the app to view the real address behind short links. So you won't have unknowingly open a malicious or unwanted links. UntrackMe can unshorten URLs of these services.

- t.co
- nyti.ms
- bit.ly
- tinyurl.com
- goo.gl
- ow.ly
- bl.ink
- buff.ly
<br><a name="cleanup"></a><br>
<h4>Cleanup</h4>

&nbsp;&nbsp;&nbsp;&nbsp;Some URLs on the internet have unnecessary parameters that are only used for tracking. UTM parameters are an example for this. UntrackMe can remove a range of known tracking parameters from URLs


### How-to

If you have a URL somewhere in your phone, open it with UntrackMe or share the URL to the app. Then UntrackMe will magically transform the URL. After that it will display a popup and let you select an app to open the new URL.

<br>

### Customize Invidious Parameters

Invidious allows to change many parts of it with URL parameters. You can use UntrackMe to customize those parameters when opening a URL. Here's how to do that:
1. Open UntrackMe.
2. Press the settings icon in Invidious section (Not the one on top).<br>
_It should open a settings screen with the title "Invidious settings"._
3. Now touch on any parameter you want to customize and choose an option.<br>
_Order of the options for every parameter is like this:_
    - **Ignore** <br>
    UntrackMe will not take any action for the parameter.
    - **Remove** <br>
    The app will remove the parameter from invidious URLs, if it already exists.
    - **[Available values]** <br>
    After the first two actions, the list will contain available values for each parameter. If you select one of these, UntrackMe will add the parameter with the value you choose. If the parameter already exists in a URL, its value will be changed to your preference.

<br><br><br>

### To make it easier to Untrack :)
You can set UntrackMe as the default app to handle URLs. You just have to select "Always" option in the "Open with" panel of Android _(The one where you have to select an app when opening a link)_

**`========== For Lite version only ==========`**<br>
1. Click on 'CONFIGURE' button in the app.<br>
_This will open Android's settings page for UntrackMe Lite._

2. First, go to "**Open by default**", then open "**Open supported links in this app**" and select "**Always allow**".<br>
_Now UntrackMe should be the default app for opening these links._

3. Then go back to the app and open the check apps page. If you see tick marks for every item, it has worked.<br>
If you still see some warning icons, just click on them. It will ask you to select an app. Now select UntrackMe and click on "**ALWAYS**".

**`============================================`**





<br>

**Source code:** [https://framagit.org/tom79/nitterizeme](https://framagit.org/tom79/nitterizeme)

**Issues tracker:** [https://framagit.org/tom79/nitterizeme/issues](https://framagit.org/tom79/nitterizeme/issues)

**Release notes:** [https://framagit.org/tom79/nitterizeme/-/tags](https://framagit.org/tom79/nitterizeme/-/tags)

### Downloads


#### F-Droid (Full links)  <a href='https://f-droid.org/app/app.fedilab.nitterizeme'><img alt='Get it on Fdroid' src='../../fdroid.png' height='100'/></a>

#### F-Droid (Lite) <a href='https://f-droid.org/app/app.fedilab.nitterizemelite'><img alt='Get it on Fdroid' src='../../fdroid.png' height='100'/></a>
