*[Home](../../home) > [Features](../../features) &gt;* Share URLs

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Share URLs

This feature allows to gather information of an article and to automatically add them to a toot.

1. **Whatever the app you use, just click on the share button** <br><br> <img src="../../res/share/share_1.png" width="300px">

2. **Then choose Fedilab** <br><br> <img src="../../res/share/share_2.png" width="300px">

3. **The app will automatically add the title, a summary and a small picture** <br><br> <img src="../../res/share/share_3.png" width="300px">
